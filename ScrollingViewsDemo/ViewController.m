//
//  ViewController.m
//  ScrollingViewsDemo
//
//  Created by James Cash on 15-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic,strong) UIImageView *airpodsImageView;

@property (nonatomic,weak) UIView *phoneZoomingView;

@end

@implementation ViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.minimumZoomScale = /*0.5*/ 1.0;
    self.scrollView.maximumZoomScale = /*2.0*/ 1.0;
//    [self setUpAirpodImage];
    [self setUpPhoneImages];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Image setup

- (void)setUpAirpodImage
{
    self.airpodsImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"airpods.jpg"]];
    [self.scrollView addSubview:self.airpodsImageView];
    self.scrollView.contentSize = self.airpodsImageView.bounds.size;
}

- (void)setUpPhoneImages
{
    NSArray *imageNames = @[@"black", @"gold", @"jetblack", @"rosegold", @"silver"];
    CGRect screenFrame = self.view.frame;
    // for zooming:
    UIView *container = [[UIView alloc] init];
    for (NSString *name in imageNames) {
        UIImage *img = [UIImage imageNamed:[name stringByAppendingString:@".png"]];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.frame = screenFrame;
        screenFrame.origin = CGPointMake(screenFrame.origin.x + screenFrame.size.width,
                                         0);
        [container addSubview:imgView];
        self.phoneZoomingView = imgView;
    }
    [self.scrollView addSubview:container];
    self.scrollView.contentSize = CGSizeMake(screenFrame.size.width * imageNames.count, screenFrame.size.height);
    self.scrollView.pagingEnabled = YES;
//    self.phoneZoomingView = container;
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    // for airpods
//    return self.airpodsImageView;
// for iPhones
    return self.phoneZoomingView;
}
@end
